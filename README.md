This project collects most Verilog code, python auxiliary scripts, and Vivado projects implemented and used by Andrei Militaru during his PhD at the Photonics Laboratory at ETH Zurich. Disclaimer: the original form of this project is build on the RedpitayaPLL project of Felix Tebbenjohanns, at this repository still contains some of his original code. 
Should you have any question regarding this project, please contact me at andreimi@ethz.ch or at andrei.militaru@gmail.com

Ad the moment, there is still some disorder in the way the files and scripts are distributed between different folders, so I will describe the structure in some detail below. 

The most relevant folders for any user are the following:
1) python_scripts: this folder contains most of the python scripts used to automatically generate long Verilog scripts, such as look-up tables (LUTs) or delay modules.
2) scripts: this folder contains the .tcl scripts necessary to build the desired project from zero.
3) vhdl: this name is actually a misnomer. In reality, this folder contains all the relevant sources for the Vivado project, including both Verilog and VHDL.


Besides the mentioned folders, the way a project is constructed from scratch follows the same scheme:
-- Open Vivado. In principle the version does not matter, however it may be necessary to use 2019.1 in order to avoid some silly errors at first.
-- Navigate via the command line to the main folder of this repository.
-- Run, for instance, the command source ./create_kovacs_project.tcl
